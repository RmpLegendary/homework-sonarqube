package ra.controllers;

import ra.models.*;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class TypicodeCtrl {

    private RequestSpecification requestSpec;
    private ResponseSpecification responseSpec;
    private String baseUrl = "https://my-json-server.typicode.com";
    private String basePath = "/typicode/demo";

    /**
     * Initialize Rest Assured
     */
    public TypicodeCtrl() {
        requestSpec = new RequestSpecBuilder()
                .log(LogDetail.ALL)
                .setBaseUri(baseUrl)
                .setBasePath(basePath)
                .setContentType(ContentType.JSON)
                .build();

        responseSpec = new ResponseSpecBuilder()
                .expectContentType(ContentType.JSON)
                .build();
        responseSpec.logDetail(LogDetail.ALL);
    }

    public Response createPost(PostModel post) {
        return given()
//                .body(post)
                .spec(requestSpec)
                .response()
                .spec(responseSpec)
        .then()
                .request().post("/posts");
    }

    /**
     * Get all posts
     *
     * @return list of posts
     */
    public PostsModel getPosts() {
        PostsModel posts;

        Response response = given(requestSpec, responseSpec).get("/posts");

        // check response
        expected200(response);

        try {
            List<PostModel> postModelList = response.jsonPath().getList("", PostModel.class);
            posts = new PostsModel(postModelList);
        } catch (Exception e) {
            posts = null;
        }

        return posts;
    }

    /**
     * Get post by ID
     *
     * @param id post ID
     * @return post
     */
    public PostModel getPost(int id) {
        PostModel post;

        Response response = given(requestSpec, responseSpec).get("/posts/" + id);

        // check response
        expected200(response);

        try {
            post = response.as(PostModel.class);
        } catch (Exception e) {
            post = null;
        }

        return post;
    }

    /**
     * Update post by ID
     *
     * @param post PostModel
     * @return response object
     */
    public Response updatePost(PostModel post) {
        return given()
                .body(post)
                .pathParam("id", post.getId())
                .spec(requestSpec)
                .response().spec(responseSpec)
        .then()
                .request().put("/posts/{id}");
    }

    /**
     * Delete post by ID
     *
     * @param id ID of post
     * @return response object
     */
    public Response deletePost(int id) {
        return given()
                .spec(requestSpec)
                .pathParam("id", id)
                .response()
                .spec(responseSpec)
        .then()
                .request().delete("/posts/{id}");
    }

    /**
     * Create new comment
     *
     * @param comment CommentModel
     * @return response object
     */
    public Response createComment(CommentModel comment){
        return given()
                .body(comment)
                .spec(requestSpec)
                .response()
                .spec(responseSpec)
        .then()
                .request().post("/comments");
    }

    /**
     * Get all comments
     *
     * @return list of comments
     */
    public CommentsModel getComments() {
        CommentsModel comments;

        Response response = given(requestSpec, responseSpec).get("/comments");

        // check response
        expected200(response);

        try {
            List<CommentModel> commentModelList = response.jsonPath().getList("", CommentModel.class);
            comments = new CommentsModel(commentModelList);
        } catch (Exception e) {
            comments = null;
        }

        return comments;
    }

    /**
     * Get comment by ID
     *
     * @param id comment ID
     * @return comment
     */
    public CommentModel getComment(Integer id) {
        CommentModel comment;

        Response response = given()
                .spec(requestSpec)
                .pathParam("id", id)
                .response()
                .spec(responseSpec)
        .then()
                .request().get("/comments/{id}");

        // check id
        expected200(response);

        try {
            comment = response.as(CommentModel.class);
        } catch (Exception e) {
            comment = null;
        }

        return comment;
    }

    /**
     * Update comment
     *
     * @param comment
     * @return resopnse object
     */
    public Response updateComment(CommentModel comment){
        return given()
                .body(comment)
                .pathParam("id", comment.getId())
                .spec(requestSpec)
                .response()
                .spec(responseSpec)
        .then()
                .request().put("/comments/{id}");
    }

    /**
     * Delete comment by ID
     *
     * @param id comment ID
     * @return resopnse object
     */
    public Response deleteComment(int id){
        return given()
                .pathParam("id", id)
                .spec(requestSpec)
                .response()
                .spec(responseSpec)
        .then()
                .request().delete("/comments/{id}");
    }

    /**
     * Get profile
     *
     * @return Profile object
     */
    public ProfileModel getProfile(){
        ProfileModel profile;

        Response response = given(requestSpec, responseSpec).get("/profile");

        expected200(response);

        try {
            profile = response.as(ProfileModel.class);
        } catch (Exception e){
            profile = null;
        }

        return profile;
    }

    /**
     * Update profile
     *
     * @param profile
     * @return resopnse object
     */
    public Response updateProfile(ProfileModel profile){
        return given()
                .body(profile)
                .spec(requestSpec)
                .response()
                .spec(responseSpec)
        .request()
                .put("/profile");
    }

    /**
     * Get DB
     *
     * @return DB object
     */
    public DbModel getDb(){
        DbModel dbModel;

        Response response = given(requestSpec, responseSpec).get("/db");

        expected200(response);

        try {
            dbModel = response.as(DbModel.class);
        } catch (Exception e){
            dbModel = null;
        }

        return dbModel;
    }

    public Response validateProfile(){
        return given(requestSpec, responseSpec).get("/profile");
    }

    public Response validatePost(){
        return given(requestSpec, responseSpec).get("/posts/1");
    }

    public Response validateComment(){
        return given(requestSpec, responseSpec).get("/comments/1");
    }

    public Response validateDb(){
        return given(requestSpec, responseSpec).get("/db");
    }

    /**
     * Expect status code of response is equals 200
     *
     * @param response
     */
    private void expected200(Response response) {
        assertThat(response.statusCode(), is(200));
    }

    /**
     * Expect content type of response is equals JSON
     *
     * @param response
     */
    private void expectedJSON(Response response) {
        assertThat(response.contentType(), is(ContentType.JSON));
    }

}

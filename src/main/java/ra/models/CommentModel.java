package ra.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "body",
        "postId"
})
public class CommentModel {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("body")
    private String body;
    @JsonProperty("postId")
    private Integer postId;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("body")
    public String getBody() {
        return body;
    }

    @JsonProperty("body")
    public void setBody(String body) {
        this.body = body;
    }

    @JsonProperty("postId")
    public Integer getPostId() {
        return postId;
    }

    @JsonProperty("postId")
    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public CommentModel(){}

    public CommentModel(Integer id, String body, Integer postId) {
        this.id = id;
        this.body = body;
        this.postId = postId;
    }

    @Override
    public String toString() {
        return "PostModel{" +
                "id=" + id +
                ", body=" + body +
                ", postId=" + postId +
                '}';
//        return new ToStringBuilder(this).append("id", id).append("body", body).append("postId", postId).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).append(body).append(postId).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CommentModel) == false) {
            return false;
        }
        CommentModel rhs = ((CommentModel) other);
        return new EqualsBuilder().append(id, rhs.id).append(body, rhs.body).append(postId, rhs.postId).isEquals();
    }

}
